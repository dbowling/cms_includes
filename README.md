# UM Server Side Plugins

## Installation
Clone this repository onto your server, it has some web based documentation pages but it does not have to be in a web accessible directory

Add the path of the classes folder within your repository to the [php include path](http://www.php.net/manual/en/ini.core.php#ini.include-path)

## Usage
Plugins are documented on the [Custom Functions for Cascade site](http://www.umt.edu/_common/includes/) as well as on the [Project Ponderosa](http://www.umt.edu/web/plugins/default.php) website.