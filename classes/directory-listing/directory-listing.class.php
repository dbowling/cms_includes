<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nick
 * Date: 9/26/13
 * Time: 6:52 PM
 * To change this template use File | Settings | File Templates.
 */


use Doctrine\Common\Cache\ApcCache;

if (!class_exists('directory_listing')) {
	include('utility.class.php');
	class directory_listing
	{

		var $mustache;
		var $api_root;
		var $columns;
		var $school_id;
		var $department_id;
		var $types;
		var $title;
		var $school_info;

		function __construct($school_id, $department_id, $types = array(), $title = "Personnel Directory", $school_info = array())
		{
			$this->school_id = intval($school_id);
			$this->department_id = intval($department_id);
			$this->types = (is_array($types) ? $types : array($types));
			$this->title = $title;
			$this->school_info = array_filter($school_info);

			$path = str_replace(basename(__FILE__), "", __FILE__);
			include(url::create_url('../../../vendor/autoload.php', $path));
			Mustache_Autoloader::register();
			$this->cache = new ApcCache();
			$this->mustache = new Mustache_Engine;
			$this->api_root = "http://cas.umt.edu/facultydatabase/2013_API/server.php";
		}

		public function initialize($template = null, $columns = 3)
		{
			if ($template == "null") {
				$template = null;
			}
			if ($this->input('ID') && is_numeric($this->input('ID'))) {
				$this->profile($this->input('ID'), 'profile');
			} else {
				$this->listing($template, intval($columns));
			}
		}

		public function profile($id, $template = 'profile', $return = false)
		{
			$data = $this->http_request($this->api_root . "/id/" . $id . "/JSON");
			$data = (array)$data[0];
			if (count($this->school_info) > 0) {
				$data['show_school_info'] = true;
				$data['school_info'] = $this->school_info;
			}
			$output = $this->mustache->render($this->get_template($template), $data);
			if ($return) {
				return $output;
			} else {
				print($output);
			}
		}

		public function listing($template = null, $columns = null, $return = false)
		{
			if ($template == null && $columns != null) {
				$template = "listing-rows";
			} else if ($template == null) {
				$template = "listing";
			}
			$output = "";
			$template_html = $this->get_template($template);
			if (is_array($this->types) && count($this->types) > 0 && !is_numeric(implode(array_keys($this->types)))) {
				$output = "<h1>" . $this->title . "</h1>";
				foreach ($this->types as $key => $type_id) {
					$profiles = $this->build_template_data($columns, array($type_id), $key);

					$output = $output . $this->mustache->render($template_html, $profiles);
				}
			} else {
				$profiles = $this->build_template_data($columns);
				$output = $this->mustache->render($template_html, $profiles);
			}

			if ($return) {
				return $output;
			} else {
				print($output);
			}
		}

		public function search($template = null, $columns = 3)
		{

			if ($template == null && $columns != null) {
				$template = "listing-rows";
			} else if ($template == null) {
				$template = "listing";
			}
			$search_output = "";
			$data = array();
			if ($this->input('q')) {
				$data['term'] = $this->input('q');
				$response = $this->http_request($this->build_url($this->school_id, $this->department_id, (is_array($this->types) && count($this->types) > 0 ? implode(",", $this->types) : null)));

				$results = array_filter($response, array($this, "search_entry"));
				$profiles = $this->build_profiles($results, $this->title, $columns);
				$template_html = $this->get_template($template);
				$search_output = $this->mustache->render($template_html, $profiles);
			}
			$this->include_view("search", $data);
			print($search_output);
		}

		private function search_entry($entry)
		{
			$query = explode(" ", $this->input('q'));

			$response = false;
			foreach ($query as $search_term) {
				if (is_numeric(stripos($entry->FirstName, $search_term)) ||
					is_numeric(stripos($entry->LastName, $search_term))
				) {
					$response = true;
				}
			}
			return $response;
		}

		private function input($key)
		{
			$response = false;
			if (isset($_GET[$key])) {
				$response = utility::xss_protect($_GET[$key]);
			}
			return $response;
		}

		private function build_url($schools = null, $departments = null, $types = null, $tags = null, $format = "JSON")
		{
			$url = '';
			if ($schools != null) {
				$url .= '/school/' . $schools;
			}
			if ($departments != null) {
				$url .= '/department/' . $departments;
			}
			if ($types != null) {
				$url .= '/type/' . $types;
			}
			if ($tags != null) {
				$url .= '/tag/' . $tags;
			}
			if ($this->input('id')) {
				$url .= '/id/' . $this->input('id');
			}
			return $this->api_root . $url . "/" . $format;
		}


		private function build_template_data($columns = null, $types = null, $title = null)
		{
			if($types == null) {$types = $this->types;}
			if($title == null) {$title = $this->title;}
			$data = $this->http_request($this->build_url($this->school_id, $this->department_id, (is_array($types) && count($types) > 0 ? implode(",", $types) : null)));
			if ($columns > 0) {
				$profiles = $this->build_profiles($data, $title, $columns);
			} else {
				$profiles = array("profiles" => $data);
			}
			return $profiles;
		}

		private function build_profiles($data, $title, $columns)
		{
			$profiles = array();
			$profiles['title'] = $title;
			$row = 0;
			$count = 1;
			foreach ($data as $item) {
				$item->column_width = 12 / $columns;
				if (empty($item->Photo)) {
					$item->Photo = url::global_resources_base('/faculty-database/imx/default.jpg');
				} else {
					$item->Photo = "http://www.cas.umt.edu/facultydatabase/Files_Faculty_images/" . $item->Photo;
				}
				$profiles['data']->profiles[$row][] = $item;
				if ($count % $columns == 0) {
					$row++;
				}
				$count++;
			}
			return $profiles;
		}


		private function get_template($template)
		{
			return $this->get_local_resource("templates/" . $template . ".html");
		}

		private function include_view($file, $data = array())
		{

			print($this->mustache->render($this->get_local_resource("views/" . $file . ".php"), $data));

		}

		private function get_local_resource($file)
		{
			$path = str_replace(basename(__FILE__), "", __FILE__);
			$filename = realpath($path . $file);
			$output = $file;
			if (file_exists($filename)) {
				$output = file_get_contents($filename);
			}
			return $output;
		}

		private function http_request($url)
		{
			$key = md5($url);
			$response = null;
			if (!$this->cache->contains($key) || $this->input('clear') == 'true') {
				$response = json_decode(file_get_contents($url));
				$this->cache->save($key, $response, 3600);
			} else {
				$response = $this->cache->fetch($key);
				print("\n<!-- ** data fetched from cache. To clear cache: " . $_SERVER["REDIRECT_SCRIPT_URI"] . "?clear=true ** -->\n");
			}
			return $response;
		}
	}
}