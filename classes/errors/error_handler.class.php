<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of error_handler
 *
 * @author ns159438e
 */
if (!class_exists("error_handler")) {
	include_once("url_shortener.class.php");

	class error_handler
	{

		public $search_term;

		function __construct($url_id = 1)
		{
			$this->search_term = $this->parse_url();

			$url_shortener = new url_shortener($url_id);
			$url_shortener->redirect_lookup();
		}

		private function parse_url()
		{
			return trim(str_replace("/", " ", $_SERVER["SCRIPT_URL"]));
		}

	}

}