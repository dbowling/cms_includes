<?php

if (!class_exists("file")) {
	class file
	{

		static function file_list($directory = '.', $mode = 'html')
		{

			switch ($mode) {
				case 'xml':
					$directory_node = "directory";
					$file_node = "file";
					header("Content-Type:text/xml");
					break;
				default : // the default case is for html
					$directory_node = "ul";
					$file_node = "li";
					break;
			}

			if (is_dir($directory)) {
				if ($dh = opendir($directory)) {
					print("<" . $directory_node . ">\n");
					while (($file = readdir($dh)) !== false) {
						if (substr($file, 0, 1) != ".") {
							$is_dir = is_dir($directory . "/" . $file);

							print("\t<" . $file_node . ">\n");
							print("\t\t<a class='" . ($is_dir ? "folder" : "file") . "' href=\"http://" . $_SERVER['HTTP_HOST'] . str_replace("index.php", "", $_SERVER['SCRIPT_NAME']) . trim(htmlspecialchars(utf8_encode($directory . "/" . $file)) . "\">" . htmlspecialchars(utf8_encode($file)), "./") . "</a>");
							if ($is_dir) {
								file::file_list($directory . "/" . $file, $mode);
							}
							print("\n</" . $file_node . ">\n");
						}
					}
				}
				closedir($dh);
			}
			print("</" . $directory_node . ">\n");
		}

		private function get_file_contents($location, $username = "", $password = "")
		{

			$result = null;

			$ch = curl_init($location);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
			$html = curl_exec($ch);
			curl_close($ch);

			return $html;
		}

		function media_list($url, $user = null, $pass = null, $result = null)
		{
			$directory_node = "ul";
			$file_node = "li";
			$hidden_ext = array("php", "LCK");
			$xml = null;
			if ($url != null) {
				$url = $url . "?mode=xml";
				if (!empty($user) && !empty($pass)) {
					$contents = $this->get_file_contents($url, $user, $pass);
					$xml = @simplexml_load_string($contents);
				} else {
					$xml = simplexml_load_file($url, NULL, LIBXML_NOCDATA);
				}
			}
			if ($xml) {
				$result = $xml->xpath('/directory/file/a');
			}
			if ($result) {
				print("<" . $directory_node . ">");
				foreach ($result as $node) {
					$file_info = (array)$node;
					$anchor = $file_info["@attributes"]["href"];
					$type = $file_info["@attributes"]["class"];
					if (!in_array(end(explode(".", $file_info[0])), $hidden_ext)) {
						print("<" . $file_node . ">");
						print("<span class='" . $type . "'><a href=\"" . $anchor . "\"" . $file_info[0] . "\">" . $file_info[0] . "</a></span>");
						if ($node->xpath('../directory')) {
							$directory = $node->xpath('../directory/file/a');
							file::media_list(null, null, null, $directory);
						}
						print("</" . $file_node . ">");
					}
				}
				print("</" . $directory_node . ">");
			}
			/* print("<pre>");
			  print_r($xml);
			  print("</pre>"); */
		}

	}

}