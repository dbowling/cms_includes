<?php
if (!class_exists("Forms")) {
	include_once('ip.class.php');
	include_once('utility.class.php');
	require_once('encryption.class.php');
	include_once('phpmailerlite.class.php');
	class Forms
	{

		var $config, $acceptable_referers, $to, $utility, $styles_included;

		function __construct($config = array(), $return_output = false)
		{
			$this->encryption = new Encryption();
			$this->to = "";
			$this->ip = new ip();
			$this->utility = new utility();
			$this->styles_included = false;

			$default_config = array(
				"thanks" => "Thanks for your Response",
				"thanks_redirect" => rtrim(htmlentities($_SERVER['PHP_SELF']), '/'),
				"submit_text" => "Send",
				"required_class" => "required",
				"form_id" => "email_form",
				"ul_class" => "email_form",
				"action" => rtrim(htmlentities($_SERVER['PHP_SELF']), '/'),
				"warning_class" => "warning",
				"show_clear_form" => false,
				"clear_form_text" => "Clear",
				"acceptable_referers" => array("wts-phpdev1.ito.umt.edu", "mumwww3.gs.umt.edu", "staging.umonline.umt.edu", "umonline.umt.edu", "localhost", "staging.umt.edu", "umt.edu", "www.umt.edu", "10.10.18.121"),
				"from_field" => "",
				"subject_field" => "",
				"subject_field_value" => null,
				"captcha" => false,
				"captcha_public" => "6Lcn4c4SAAAAABWuYDIiazqPUKgjnjWSi4MgcBK3",
				"captcha_private" => "6Lcn4c4SAAAAADJtuEnQjotjbEMpJFLTHZtGPPtA",
				"db" => "mysql",
				"db_config" => array(
					"user" => "cascade_forms",
					"pass" => "Oozae5rah6Ikie",
					"host" => "wtsmysql01.ito.umt.edu",
					"port" => 3306,
					"database" => "cascade_forms")
				/*  array(
				  "db" => "mongodb",
				  "db_config" => array(
				  "user" => "root",
				  "pass" => "itopass",
				  "host" => "localhost",
				  "port" => "27017",
				  "database" => "forms")
				 *
				 */
			);
			if (empty($config['thanks'])) {
				unset($config['thanks']);
			}
			if (empty($config['thanks_redirect'])) {
				unset($config['thanks_redirect']);
			}
			if (!empty($config['from_field'])) {
				$config['from_field'] = $this->format_input_name($config['from_field']);
			}
			if (!empty($config['subject_field'])) {
				$config['subject_field'] = $this->format_input_name($config['subject_field']);
			}

			$this->config = (object)array_merge($default_config, $config);
			if ($this->config->captcha) {
				include("lib/recaptchalib.php");
			}

			$this->prepare_thanks_redirect();
		}

		function create_url($relative_url, $source_url = null)
		{
			if ($source_url == null) {
				$source_url = (isset($_SERVER["SCRIPT_URI"]) ? $_SERVER["SCRIPT_URI"] : $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
				//phpInfo();
			}
			$relative_pieces = explode("/", $source_url);
			if (end(explode(".", end($relative_pieces))) == "php") {
				unset($relative_pieces[count($relative_pieces) - 1]);
				$source_url = implode("/", $relative_pieces);
			}
			$array = explode('/', $source_url . '/' . $relative_url);
			$domain = array_shift($array);
			$parents = array();
			foreach ($array as $dir) {
				switch ($dir) {
					case '.':
						// Don't need to do anything here
						break;
					case '..':
						array_pop($parents);
						break;
					default:
						$parents[] = $dir;
						break;
				}
			}
			return $domain . '/' . implode('/', $parents);
		}

		private function prepare_thanks_redirect()
		{
			if (!is_numeric(stripos($this->config->thanks_redirect, "http"))) {
				$this->config->thanks_redirect = $this->create_url($this->config->thanks_redirect);
			}
			stream_context_set_default(array('http' => array('method' => 'HEAD')));
			$http_status = trim(preg_replace("/(.*?) (.*?) (.*)/i", "$2", end(array_slice(get_headers($this->config->thanks_redirect), 0, 1))));
			if ($http_status > 400) {
				$this->config->thanks_redirect = rtrim(htmlentities($_SERVER['PHP_SELF']), '/');
			}
			$this->config->thanks_redirect .= "?thanks=true";
		}


		public function display_email_form($fields, $to, $postvars = array(), $return_response = false)
		{
			$this->to = $to;

			$response = new stdClass();
			$response->html_output = $this->include_styles(true, true);
			if (isset($_GET['thanks']) && $_GET['thanks'] == 'true') {
				$response->html_output .= "<div class='thanks info'><p>" . $this->config->thanks . "</p></div>";
			} else if (isset($postvars['send']) && $postvars['send'] == "true") {
				$postvars = $this->evaluate_custom_fields($postvars);
				$validation = $this->validate($fields, $postvars);
				if (count($validation) > 0) {
					$response->html_output .= $this->build_form($fields, $postvars, $validation);
				} else {
					$response = $this->process_data($postvars);
					if (!headers_sent()) {
						header('Location: ' . $this->config->thanks_redirect);
					} else {
						$response->html_output .= "<div class='thanks info'>" . $this->config->thanks . "</div>";
					}
				}
			} else if (isset($postvars['clear']) && $postvars['clear'] == "true") {
				unset($postvars['send']);
				$_SESSION = array();
				$response->html_output .= $this->build_form($fields, $postvars);
			} else {
				unset($postvars['send']);
				$response->html_output .= $this->build_form($fields, $postvars);
			}
			if ($return_response) {
				return $response;
			} else {
				print($response->html_output);
			}
		}

		private function evaluate_custom_fields($postvars)
		{
			$dates = array();
			$times = array();
			$sets = array();
			$keys = array();
			foreach ($postvars as $key => $value) {
				$postvar_key = str_ireplace(array("set_","date_", "time_", "_hour", "_minute", "_meridiem", "_month", "_day", "_year"), "", $key);
				$postvar_key = preg_replace("/_item_[0-9]/","", $postvar_key);
				if (is_numeric(stripos($key, "date_"))) {
					$date_key = explode("_", $key);
					$dates[$postvar_key][end($date_key)] = $value;
					$keys[] = $key;
				} else if (is_numeric(stripos($key, "time_"))) {
					$time_key = explode("_", $key);
					$times[$postvar_key][end($time_key)] = $value;
					$keys[] = $key;
				} else if (is_numeric(stripos($key, "set_"))) {
					$set_key = explode("_", $key);
					$sets[$postvar_key][end($set_key)] = $value;
					$keys[] = $key;
				}
			}
			foreach ($keys as $key) {
				unset($postvars[$key]);
			}

			foreach ($sets as $key => $set) {
				$postvars[$key] = implode(", ", $set);
			}


			foreach ($times as $key => $time) {
				$postvars[$key] = $time['hour'] . ":" . ($time['minute'] == 0 ? "00 " : $time['minute']) . $time['meridiem'];
			}

			foreach ($dates as $key => $date) {
				$postvars[$key] = $date['day'] . "/" . $date['month'] . "/" . $date['year'];
			}


			return $postvars;
		}

		function include_styles($return = false, $forse_include = false)
		{
			$output = "";
			if ($this->styles_included == false || $forse_include == true) {
				$output = "<link href='" . $this->utility->site_url() . "/_common/includes/resources/email_form.css' rel='stylesheet' type='text/css' />\n";
				$this->styles_included = true;
			}
			if ($return) {
				return $output;
			} else {
				print($output);
			}
		}

		function referer_check()
		{
			$referer = $_SERVER["HTTP_REFERER"];
			$start = stripos($referer, "://") + 3;
			$domain = substr($referer, $start, stripos($referer, "/", $start) - $start);
			return in_array($domain, $this->config->acceptable_referers);
		}

		function process_data($postvars)
		{
			$recipients = preg_split("/[\s,;]+/", $this->to);
			$from = $recipients[0];
			if (isset($postvars[$this->config->from_field]) && $postvars[$this->config->from_field] != "") {
				$from = $postvars[$this->config->from_field];
			}
			if (isset($postvars[$this->config->subject_field]) && $postvars[$this->config->subject_field] != "") {
				$this->config->subject_field_value = $postvars[$this->config->subject_field];
			} else if (!empty($this->config->subject_field)) {
				foreach ($postvars as $key => $value) {
					$this->config->subject_field = $this->format_input_name(str_replace("{{" . $this->unformat_input_name($key) . "}}", $value, $this->unformat_input_name($this->config->subject_field)));

				}
				$this->config->subject_field_value = $this->unformat_input_name($this->config->subject_field);
			}
			$body = "";
			$alt_body = "";
			$input_data = new stdClass();
			foreach ($postvars as $key => $value) {
				$key = trim($this->utility->xss_protect($key));
				$value = trim($this->process_input($value));
				if (is_numeric(stripos($key, 'heading'))) {
					$body .= "<h2>" . $value . "</h2>";
					$alt_body .= "** " . $value . " **\n";
				} else if ($key != 'send') {
					$body .= "<p><strong>\n" . $this->unformat_input_name($key) . "</strong>: " . $value . "\n</p>";
					$alt_body .= $this->unformat_input_name($key) . ": " . $value . "\n";
					$input_data->$key = $value;
				}
			}
			$user_info = $this->user_meta_info();
			$body .= $user_info;
			$alt_body .= strip_tags($user_info);
			$response = new stdClass();

			$response->data = $input_data;
			$response->db_insert_id = $this->persist_data($input_data);
			$response->mail_response = $this->send_email($recipients, $from, $body, $alt_body);
			return $response;
		}

		function process_input($input)
		{
			$input = $this->utility->xss_protect($input);
			$input = utf8_encode($input);
			return $input;
		}

		function send_email($recipients, $from, $body, $alt_body)
		{

			$mail = new PHPMailerLite();
			$mail->SingleTo = false;
			$mail->IsMail();
			foreach ($recipients as $recipient) {
				$mail->AddAddress($recipient);
			}
			$mail->SetFrom($from);
			$mail->Subject = ($this->config->subject_field_value != "" ? ($this->config->subject_field_value) : "[web form] " . $this->config->form_id);


			$mail->AltBody = $alt_body;
			$mail->MsgHTML($body);
			return $mail->Send();
		}

		private function get_site_name()
		{
			$site_segments = explode("/", trim($_SERVER['SCRIPT_NAME'], '/'));

			if (count($site_segments) == 1) {
				$site = str_ireplace("www.", "", $_SERVER['HTTP_HOST']);
			} else {
				$site = $site_segments[0];
			}
			return $site;
		}

		private function persist_data($data)
		{
			$db_data = new stdClass();
			$site = $this->get_site_name();
			$db_data->meta = $this->build_meta_data();
			$insert_id = null;
			$site_hash = md5($site);
			$form_hash = md5($site . $db_data->meta['form_name']);
			//format keys to be human readable
			foreach ($data as $key => $value) {
				$key = $this->format_input_name($this->unformat_input_name($key), false);
				$db_data->$key = $value;
			}
			$version_hash = md5(implode(array_keys((array)$db_data)));


			switch ($this->config->db) {
				case "mongodb":
					$insert_id = $this->save_data_mongodb($site, $db_data, $site_hash, $form_hash, $version_hash);
					break;
				case "mysql":
					$insert_id = $this->save_data_mysql($site, $db_data, $site_hash, $form_hash, $version_hash);
					break;
			}
			return $insert_id;
		}

		private function save_data_mysql($site, $data, $site_hash, $form_hash, $version_hash)
		{
			$insert_id = false;
			if (class_exists('mysqli') && isset($this->config->db_config['user'])) {
				$mysqli = new mysqli($this->config->db_config['host'],
					$this->config->db_config['user'],
					$this->config->db_config['pass'],
					$this->config->db_config['database'],
					$this->config->db_config['port']);

				if ($mysqli->connect_error === null) {
					$serialized_data = addslashes(json_encode($data));
					$sql = "INSERT INTO entries (`site`,`data`,`form_name`,`site_hash`,`form_hash`,`version_hash`)
                        values (\"" . $site . "\",\"" . $serialized_data . "\", \"" . $data->meta['form_name'] . "\",\"" . $site_hash . "\",\"" . $form_hash . "\",\"" . $version_hash . "\");";
					$mysqli->query($sql);
					$insert_id = $mysqli->insert_id;
				}
				$mysqli->close();
			}
			return $insert_id;
		}

		private function save_data_mongodb($site, $data, $site_hash, $form_hash, $version_hash)
		{
			$response = false;
			if (class_exists('Mongo') && isset($this->config->db_config['user'])) {
				$database = $this->config->db_config['database'];
				$mongo = new Mongo("mongodb://" . $this->config->db_config['user'] . ":" . $this->config->db_config['pass'] . "@" . $this->config->db_config['host'] . ":" . $this->config->db_config['port']);
				$db = $mongo->$database; //site
				$collection = $db->{$site}; //form
				$response = $collection->insert($data, true);
			}
			return $response;
		}

		private function build_meta_data()
		{
			global $netid;
			$site_segments = explode("/", trim($_SERVER['SCRIPT_NAME'], '/'));
			unset($site_segments[0]);
			$form_id = implode("_", $site_segments) . '_' . $this->config->form_id;
			$platform = $this->getBrowser();
			$meta = array(
				"emailed_to" => $this->to,
				"browser" => $platform->browser . " " . $platform->version,
				"operating_system" => $platform->platform,
				"ip_address" => $this->ip->ip_address(),
				"sent_date" => date('r'),
				"sent_from" => "http://" . $_SERVER["HTTP_HOST"] . $_SERVER['REQUEST_URI'],
				"referer" => $_SERVER["HTTP_REFERER"],
				"form_name" => $form_id
			);
			if (!empty($netid)) {
				if (function_exists("ldap_bind")) {
					$oid = new oid();
					$user_info = (array)$oid->get_user_by_netid($netid);
					$meta = array_merge($meta, $user_info);
				} else {
					$meta['netid'] = $netid;
				}
			}
			return $meta;
		}

		private function user_meta_info()
		{
			$metadata = $this->build_meta_data();

			$content = "\n\n<div style='border: 1px solid #999; background-color: #eee;'><h4 style='margin: 3px;'>Information About The Sender:</h4>";
			foreach ($metadata as $key => $value) {
				if (!empty($value)) {
					$label = ucwords(str_replace("_", " ", $key));
					$content .= "\t<p style='margin: 3px;'>\n" . $label . ": " . $value . "\n</p>";
				}
			}
			$content .= "</div>";
			return $content;
		}

		private function validate($fields, $postvars)
		{

			$invalid_fields = array();
			foreach ($fields as $field) {
				$field_id = $this->format_input_name($field['name']);
				if (isset($postvars[$field_id])) {
					$_SESSION[$field_id] = $postvars[$field_id];
				}
				if ($field["type"] != 'check_box' && $field["type"] != 'heading' && $field["type"] != 'message' && $field["required"] == true && empty($postvars[$field_id])) {
					$invalid_fields[] = $field_id;
				}
			}
			if ($this->config->captcha) {
				$response = recaptcha_check_answer($this->config->captcha_private, $_SERVER["REMOTE_ADDR"], $postvars["recaptcha_challenge_field"], $postvars["recaptcha_response_field"]);
				if (!$response->is_valid) {
					$invalid_fields[] = "Recaptcha";
				}
			}
			if ($this->referer_check() != true) {
				$invalid_fields[] = "Referer Check";
			}
			return $invalid_fields;
		}


		private function format_input_name($raw_input, $encrypt = true)
		{
			$format = "";
			if ($encrypt) {
				$format = $this->encryption->encrypt($raw_input);
			} else {
				$parsed_name = str_replace(" ", "_", $raw_input);
				$format = str_replace("'", "|", $parsed_name);

			}

			return $format;
		}

		private function unformat_input_name($raw_input, $encrypt = true)
		{
			$format = "";
			if ($encrypt) {
				$format = $this->encryption->decrypt($raw_input);
			} else {
				$parsed_name = str_replace("_", " ", $raw_input);
				$format = str_replace("|", "'", $parsed_name);

			}

			return $format;
		}

		private function build_form($fields, $postvars = array(), $validation_issues = array())
		{
			$form_html = "<div class='form_wrapper'>";
			if (count($validation_issues) > 0) {
				$issues = array();
				foreach ($validation_issues as $issue) {
					$issues[] = $this->unformat_input_name($issue);
				}
				$form_html .= "<p class='" . $this->config->warning_class . "'>The following fields are required: " . implode(", ", $issues) . "</p>";
			}
			$form_html .= "<form action='" . rtrim($this->config->action, "/") . "' method='post' id='" . $this->config->form_id . "'><ul class='" . $this->config->ul_class . "'>";

			foreach ($fields as $field) {
				$required_class = "";
				$class = "";
				$value = "";
				$label = "";
				$help_text_html = "";
				$form_element = "";

				$input_name = $this->format_input_name($field['name']);

				if (isset($field['required']) && $field['required'] == true) {
					$required_class = $this->config->required_class;
				}
				if (isset($field['class']) && trim($field['class']) != "") {
					$class = ' ' . $field['class'];
				}

				if ($field['type'] != "hidden" && isset($postvars[$input_name]) && trim($postvars[$input_name]) != "") {
					$value = $postvars[$input_name];
				} else if ($field['type'] != "hidden" && isset($_SESSION[$input_name])) {
					$value = $_SESSION[$input_name];
				} else if (isset($field['value'])) {
					$value = $field['value'];
				}
				$help_text = "";
				if (!empty($field['help_text'])) {
					$help_text = $field['help_text'];
					$help_text_html = "<br/><span class='help_text'>" . $help_text . "</span>\n";
				}

				$label = "<label class='question' for='" . $input_name . "'>" . $field['name'] . $help_text_html . (!empty($required_class) ? "<span class='access_hidden'> (required)</span>" : "") . "</label>";
				$form_html .= "<li class='" . $required_class . " " . str_replace(" ", "_", $field['type']) . "'>";
				switch ($field['type']) {
					case 'hidden':
						$label = "";
						$form_element = "<input type='" . $field['type'] . "' name='" . $input_name . "' id='" . $input_name . "'  value='" . $value . "' class='" . $class . "' title='" . $help_text . "'/>";
						break;
					case 'text':
						$form_element = "<input type='" . $field['type'] . "' name='" . $input_name . "' id='" . $input_name . "' value='" . $value . "' class='" . $class . "' title='" . $help_text . "'/>";
						break;
					case 'date':
						$month = "<div class='datetime_option'><label for='" . $input_name . "_month'>Month</label>";
						$month .= "<select name='date_" . $input_name . "_month'' id='" . $input_name . "_month' title='" . $help_text . "'>";
						$month .= $this->get_options_by_name("Month", $this->unformat_input_name($value));
						$month .= "</select></div>";

						$day = "<div class='datetime_option'><label for='" . $input_name . "_day'>Day</label>";
						$day .= "<select name='date_" . $input_name . "_day'' id='" . $input_name . "_day' title='" . $help_text . "'>";
						$day .= $this->get_options_by_name("Day", $this->unformat_input_name($value));
						$day .= "</select></div>";

						$year = "<div class='datetime_option'><label for='" . $input_name . "_year'>Year</label>";
						$year .= "<select name='date_" . $input_name . "_year'' id='" . $input_name . "_year' title='" . $help_text . "'>";
						$year .= $this->get_options_by_name("Year", $this->unformat_input_name($value));
						$year .= "</select></div>";

						$form_element = "<div class='" . $class . " date_input'>" . $month . $day . $year . "</div>";
						break;
					case 'time':
						$hour = "<div class='datetime_option'><label for='" . $input_name . "_hour'>Hour</label>";
						$hour .= "<select name='time_" . $input_name . "_hour'' id='" . $input_name . "_hour' title='" . $help_text . "'>";
						$hour .= $this->get_options_by_name("hour", $this->unformat_input_name($value));
						$hour .= "</select></div>";

						$minute = "<div class='datetime_option'><label for='" . $input_name . "_minute'>Minute</label>";
						$minute .= "<select name='time_" . $input_name . "_minute'' id='" . $input_name . "_minute' title='" . $help_text . "'>";
						$minute .= $this->get_options_by_name("minute", $this->unformat_input_name($value));
						$minute .= "</select></div>";

						$meridiem = "<div class='datetime_option'><label for='" . $input_name . "_meridiem'>AM/PM</label>";
						$meridiem .= "<select name='time_" . $input_name . "_meridiem'' id='" . $input_name . "_meridiem' title='" . $help_text . "'>";
						$meridiem .= $this->get_options_by_name("meridiem", $this->unformat_input_name($value));
						$meridiem .= "</select></div>";

						$form_element = "<div class='" . $class . " time_input'>" . $hour . $minute . $meridiem . "</div>";
						break;
					case 'radio set':
						$form_element = "\n<div class='radio" . $class . "'>";
						if (count($field['options']) > 0) {
							foreach ($field['options'] as $option) {
								$form_element .= "<div class='option'>";
								$form_element .= "<input type='radio' name='" . $input_name . "' value='" . $option . "' class='" . $class . "' title='" . $help_text . "'/>";
								$form_element .= "\n\t<label for='" . $input_name . "'>" . "<span class='access_hidden'>" . $field['name'] . "</span>" . $option . "</label>";
								$form_element .= "</div>";
							}
						}
						$form_element .= "\n</div>";
						break;
					case 'checkbox':
						if (isset($field['options']) && count($field['options']) > 0) {
							$form_element = "\n<div class='checkbox" . $class . "'>";
							$count = 1;
							foreach ($field['options'] as $option) {
								$selected = "";
								if ($option == $this->unformat_input_name($value)) {
									$selected = "checked";
								}
								$form_element .= "<div class='option'>";
								$form_element .= "\n\t<input name='set_" . $input_name . "_item_" . $count . "' id='set_" . $input_name . "_item_" . $count . "' " . $selected . " type='" . $field['type'] . "' value='" . $option . "'/>";
								$form_element .= "\n\t<label for='set_" . $input_name . "_item_" . $count . "'>" . "<span class='access_hidden'>" . $field['name'] . "</span>" . $option . "</label>";
								$form_element .= "</div>";
								$count++;
							}
							$form_element .= "\n</div>";
						} else {
							$form_element = "\n<div class='single_checkbox'>";
							$form_element .= $label;
							$form_element .= "<input type='" . $field['type'] . "' name='" . $input_name . "' id='" . $input_name . "'  value='Yes' class='" . $class . "' title='" . $help_text . "'/>";
							$form_element .= "</div>";
							$label = "";
						}
						break;
					case 'textarea':
						$form_element = "<textarea name='" . $input_name . "' id='" . $input_name . "' class='" . $class . "' title='" . $help_text . "'>" . $value . "</textarea>";
						break;
					case 'dropdown':
						$form_element = "<select name='" . $input_name . "' id='" . $input_name . "' class='" . $class . "' title='" . $help_text . "'>";
						if (count($field['options']) > 0) {
							foreach ($field['options'] as $option) {
								$selected = "";
								if ($option == $this->unformat_input_name($value)) {
									$selected = "selected";
								}

								$form_element .= "<option value='" . $option . "' " . $selected . ">" . $option . "</option>";
							}
						} else {
							$form_element .= $this->get_options_by_name($field['name'], $this->unformat_input_name($value));
						}
						$form_element .= "</select>";
						break;
					case 'heading':
						$label = "";
						$form_element = "<h2 class='" . $class . "'>" . $field['name'] . "</h2>";
						$form_element .= "<input type='hidden' name='heading_" . md5($field['name']) . "' value='" . $field['name'] . "' />";
						if (!empty($help_text)) {
							$help_text_html = "<p>" . $help_text . "</p>";
						}
						break;
					case 'message':
						$label = "";
						$form_element = "<p class='" . $class . "'>" . $field['name'] . "</p>";
						if (!empty($help_text)) {
							$help_text_html = "<p>" . $help_text . "</p>";
						}
						break;
				}
				$form_html .= $label . $form_element;
				$form_html .= "</li>\n";
			}

			$form_html .= "</li>
                </ul>";
			if ($this->config->captcha) {
				$form_html .= recaptcha_get_html($this->config->captcha_public) . "<div class='submit'>";
			}
			$form_html .= "
                <input type='hidden' name='send' value='true'/>
                <input type='submit' class='submit_button' value='" . $this->config->submit_text . "'/>
           </form>
           ";

			if ($this->config->show_clear_form) {
				$form_html .= "<form action='" . $this->config->action . "' method='post' id='clear_" . $this->config->form_id . "'>
                <div class='clear'>
                <input type='hidden' name='clear' value='true'/>
                <input class='clear' type='submit' value='" . $this->config->clear_form_text . "'/>
                </div>
            </form>
                    ";
			}
			$form_html .= "</div>";
			return $form_html;
		}

		private function get_options_by_name($field_name, $selected_value)
		{
			$options_html = "";
			$options = array();

			switch (strtolower($field_name)) {
				case 'month':
					$options = range(1, 12);
					break;
				case 'day':
					$options = range(1, 31);
					break;
				case 'meridiem':
					$options = array("AM", "PM");
					break;
				case 'hour':
					$options = range(1, 12);
					break;
				case 'minute':
					$options = range(00, 59, 15);
					break;
				case 'year':
					if ($selected_value == "") {
						$selected_value = date("Y");
					}
					$start_date = date("Y") - 4;
					for ($i = 0; $i < 9; $i++) {
						$options[] = $start_date + $i;
					}
					break;
				case 'state':
					$options = array('AL' => "Alabama", 'AK' => "Alaska", 'AZ' => "Arizona", 'AR' => "Arkansas", 'CA' => "California", 'CO' => "Colorado", 'CT' => "Connecticut", 'DE' => "Delaware", 'DC' => "District Of Columbia", 'FL' => "Florida", 'GA' => "Georgia", 'HI' => "Hawaii", 'ID' => "Idaho", 'IL' => "Illinois", 'IN' => "Indiana", 'IA' => "Iowa", 'KS' => "Kansas", 'KY' => "Kentucky", 'LA' => "Louisiana", 'ME' => "Maine", 'MD' => "Maryland", 'MA' => "Massachusetts", 'MI' => "Michigan", 'MN' => "Minnesota", 'MS' => "Mississippi", 'MO' => "Missouri", 'MT' => "Montana", 'NE' => "Nebraska", 'NV' => "Nevada", 'NH' => "New Hampshire", 'NJ' => "New Jersey", 'NM' => "New Mexico", 'NY' => "New York", 'NC' => "North Carolina", 'ND' => "North Dakota", 'OH' => "Ohio", 'OK' => "Oklahoma", 'OR' => "Oregon", 'PA' => "Pennsylvania", 'RI' => "Rhode Island", 'SC' => "South Carolina", 'SD' => "South Dakota", 'TN' => "Tennessee", 'TX' => "Texas", 'UT' => "Utah", 'VT' => "Vermont", 'VA' => "Virginia", 'WA' => "Washington", 'WV' => "West Virginia", 'WI' => "Wisconsin", 'WY' => "Wyoming");
					break;
			}
			foreach ($options as $option) {
				$selected = "";
				if ($option == $selected_value) {
					$selected = "selected";
				}
				$options_html .= "<option value='" . $option . "' " . $selected . ">" . $option . "</option>\n";
			}
			return $options_html;
		}

		private function getBrowser()
		{
			$u_agent = $_SERVER['HTTP_USER_AGENT'];
			$bname = 'Unknown';
			$platform = 'Unknown';
			$version = "";

			//First get the platform?
			if (preg_match('/linux/i', $u_agent)) {
				$platform = 'linux';
			} elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
				$platform = 'mac';
			} elseif (preg_match('/windows|win32/i', $u_agent)) {
				$platform = 'windows';
			}

			// Next get the name of the useragent yes seperately and for good reason
			if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
				$bname = 'Internet Explorer';
				$ub = "MSIE";
			} elseif (preg_match('/Firefox/i', $u_agent)) {
				$bname = 'Mozilla Firefox';
				$ub = "Firefox";
			} elseif (preg_match('/Chrome/i', $u_agent)) {
				$bname = 'Google Chrome';
				$ub = "Chrome";
			} elseif (preg_match('/Safari/i', $u_agent)) {
				$bname = 'Apple Safari';
				$ub = "Safari";
			} elseif (preg_match('/Opera/i', $u_agent)) {
				$bname = 'Opera';
				$ub = "Opera";
			} elseif (preg_match('/Netscape/i', $u_agent)) {
				$bname = 'Netscape';
				$ub = "Netscape";
			}

			// finally get the correct version number
			$known = array('Version', $ub, 'other');
			$pattern = '#(?<browser>' . join('|', $known) .
				')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
			if (!preg_match_all($pattern, $u_agent, $matches)) {
				// we have no matching number just continue
			}

			// see how many we have
			$i = count($matches['browser']);
			if ($i != 1) {
				//we will have two since we are not using 'other' argument yet
				//see if version is before or after the name
				if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
					$version = $matches['version'][0];
				} else {
					$version = $matches['version'][1];
				}
			} else {
				$version = $matches['version'][0];
			}

			// check if we have a number
			if ($version == null || $version == "") {
				$version = "?";
			}

			return (object)array(
				'userAgent' => $u_agent,
				'browser' => $bname,
				'version' => $version,
				'platform' => $platform,
				'pattern' => $pattern
			);
		}

	}
}
