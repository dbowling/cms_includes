<?php

class oid {

    var $oid_connection;
    var $base_dn;

    function __construct() {

        $user = 'opt1admin';
        $userdn = 'cn=' . $user . ',ou=people,dc=umt,dc=edu';
        $password = 'opt1admin';
        $host = 'cidp.umt.edu';
        $this->basedn = 'ou=people,dc=umt,dc=edu';
        $this->oid_connection = ldap_connect("ldap://{$host}") or die('Could not connect to LDAP server.');
        $this->attributes = array('cn', 'sn', 'givenname', 'uid', 'umid', 'activitycode', 'mail');

        ldap_set_option($this->oid_connection, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($this->oid_connection, LDAP_OPT_REFERRALS, 0);
        ldap_bind($this->oid_connection, $userdn, $password);
    }

    function __destruct() {
        ldap_unbind($this->oid_connection);
    }

    function oid_last_name_search($last_name) {
        $result = ldap_search($this->oid_connection, $this->basedn, "(&(sn={$last_name})(activitycode=A))", $this->attributes);
        $entries = null;
        $users = array();
        if ($result != FALSE) {
            $entries = ldap_get_entries($this->oid_connection, $result);
            foreach ($entries as $entry) {
                if (!empty($entry['uid'][0])) {
                    $users[$entry['uid'][0]] = new user($entry['uid'][0], $entry['cn'][0], $entry['sn'][0], $entry['givenname'][0], $entries[0]['mail'][0]);
                }
            }
            sort($users);
        }
        return $users;
    }

    function get_user_by_umid($id) {
        $result = ldap_search($this->oid_connection, $this->basedn, "(umid={$id})", $this->attributes);

        $user_info = null;
        if ($result != false) {
            $entries = ldap_get_entries($this->oid_connection, $result);
            if ($entries['count'] > 0) {
                $user_info = new user($entries[0]['uid'][0], $entries[0]['cn'][0], $entries[0]['sn'][0], $entries[0]['givenname'][0], $entries[0]['mail'][0]);
                foreach ($entries as $entry) {
                    if ($entry['activitycode'][0] == "A" && is_numeric(end(str_split(strtolower($entry['uid'][0]))))) {
                        $user_info->is_student = 1;
                    } else {
                        $user_info->is_employee = 1;
                    }
                }
            }
        }
        return $user_info;
    }

    function get_user_by_netid($id) {
        $result = ldap_search($this->oid_connection, $this->basedn, "(uid={$id})", $this->attributes);

        $user_info = null;
        if ($result != false) {
            $entries = ldap_get_entries($this->oid_connection, $result);
            if ($entries['count'] > 0) {
                $user_info = new user($entries[0]['uid'][0], $entries[0]['cn'][0], $entries[0]['sn'][0], $entries[0]['givenname'][0], $entries[0]['mail'][0]);
                foreach ($entries as $entry) {
                    if ($entry['activitycode'][0] == "A" && is_numeric(end(str_split(strtolower($entry['uid'][0]))))) {
                        $user_info->is_student = 1;
                    } else {
                        $user_info->is_employee = 1;
                    }
                }
            }
        }
        return $user_info;
    }

}

class user {

    var $netid;
    var $full_name;
    var $last_name;
    var $first_name;
    var $email;

    function __construct($netid, $full_name, $last_name, $first_name, $email) {
        $this->netid = $netid;
        $this->full_name = $full_name;
        $this->last_name = $last_name;
        $this->first_name = $first_name;
        $this->email = $email;
    }

}
