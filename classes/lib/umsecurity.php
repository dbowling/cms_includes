<?php

/**
 * Wrapper for phpCAS tied in with Grouper groups
 * @author Nick Shontz <nick.shontz@umontana.edu>
 *
 */
if (!class_exists("Umsecurity")) {

	class Umsecurity
	{

		var $cas_url;
		var $cas_port;
		var $cas_path;
		var $cas_appkey;
		var $netid;

		/**
		 * Constructor for UM Security, loads all the required config data
		 */
		function __construct()
		{
			parse_str($_SERVER['QUERY_STRING'], $_GET);

			require_once('phpcas.php');

			$this->cas_url = "login.umt.edu";
			$this->cas_port = 443;
			$this->cas_logout = "https://login.umt.edu/cas/logout";
			$this->cas_path = "cas";
			$this->cas_appkey = "";
			$this->netid = null;

			// initialize phpCAS
			phpCAS::client(CAS_VERSION_2_0, $this->cas_url, $this->cas_port, $this->cas_path);
			// no SSL validation for the CAS server
			//phpCas::setDebug('cas.log');
			phpCAS::setNoCasServerValidation();
		}

		/**
		 * Destroys sessiona nd redirect to cas logout.
		 */
		function logout()
		{
			$_SESSION = array();
			header("Location: " . $this->cas_logout);
			die();
		}

		/**
		 * Does not require authentication, redirects through CAS, checks for a session and returns with netid
		 * @param string $uri_string
		 * @return string
		 */
		function get_netid($uri_string = '')
		{
			phpCAS::setFixedServiceURL($this->site_url($uri_string));
			if (phpCAS::checkAuthentication()) {
				$this->netid = phpCAS::getUser();
			}
			return $this->netid;
		}

		/**
		 * Redirects through CAS, checks for a session and returns with netid
		 * @param string $uri_string
		 * @return string
		 */
		function force_authentication($uri_string = '')
		{
			// force CAS authentication
			phpCAS::setFixedServiceURL($this->site_url($uri_string));
			try {
				phpCAS::forceAuthentication();
				$this->netid = phpCAS::getUser();
			} catch (Exception $e) {
				die();
			}
			return $this->netid;
		}

		private function site_url($uri = "")
		{
			if (empty($uri)) {
				$uri = explode("?", $_SERVER["REQUEST_URI"]);
				$uri = $uri[0];
			}
			$basePath = rtrim("http://" . $_SERVER["HTTP_HOST"] . $uri, "/");

			return $basePath;
		}

	}
}