<?php
// This function will output a random image tag given a place to look.
//
// 1st parameter: required, the folder where the images are located
// 2nd parameter: optional, any extra code you want inside the image tag, such as style or class information
// 4rd parameter: optional, a space seperated list of acceptible file extensions for the rotation.
// 5th parameter: optional, set to true if you want to remove the image tag
//
if (!class_exists("Random")) {

	class Random
	{
		static function img($folder = NULL, $extras = NULL, $exts = NULL, $urlonly = FALSE)
		{
			//if they didn't include a folder, send an error
			if (!$folder) {
				echo '<strong style="color: #F00;">YOU MUST SUPPLY A FOLDER PATH FOR THE IMAGE ROTATOR TO WORK.</strong>';
				return FALSE;
			}

			// Space seperated list of extensions, you probably won't have to change this.
			if (!$exts) $exts = 'jpg jpeg png gif';

			$files = array();
			$i = -1; // Initialize some variables

			//check if the folder exists here, or if we need to go up a level... only do this 10 levels deep
			$count = 0;
			while (!file_exists($folder) && $count < 10) {
				$folder = '../' . $folder;
				$count = $count + 1;
			}

			if (file_exists($folder)) {
				$handle = opendir($folder);
				$exts = explode(' ', $exts);

				while (false !== ($file = readdir($handle))) {
					foreach ($exts as $ext) { // for each extension check the extension
						if (preg_match('/\.' . $ext . '$/i', $file, $test)) { // case insensitive
							$files[] = $file; // it's good
							++$i;
						}
					}
				}

				closedir($handle); // We're not using it anymore

				mt_srand((double)microtime() * 1000000); // seed for PHP < 4.2

				$rand = mt_rand(0, $i); // $i was incremented as we went along

				$path = $folder . '/' . $files[$rand];

				if ($urlonly) {
					echo $path;
				} else {
					echo '<img src="' . $path . '" ' . $extras . '/>';
				}

				return TRUE;
			} else {
				echo '<strong style="color: #F00;">THE FOLDER SPECIFIED FOR THE RANDOM IMAGES DOES NOT EXIST.</strong>';
				return FALSE;
			}
		}

	}
}