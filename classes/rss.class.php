<?php

if (!class_exists("Rss")) {
	class Rss
	{

		static function display($feed = NULL, $limit = 10, $verbose = FALSE, $feedlink = TRUE, $fulltext = false, $char_limit = 250)
		{
			print("<link href='/_common/includes/resources/rss.css' rel='stylesheet' type='text/css' />");

			require_once('rss/rss_fetch.inc');
			if (empty($feed)) {
				print("<p>No feed URL was specified.</p>");
				return false;
			}

			$rss = @fetch_rss($feed);
			print("<ul class='rss'>");
			if ($rss) {
				for ($i = 0; $i < count($rss->items) && $i < $limit; $i++) {
					print("<li><a href='" . $rss->items[$i]['link'] . "' class='title'>" . $rss->items[$i]['title'] . "</a>");
					if ($fulltext || ($i == 0 && !empty($rss->items[$i]['description']) && $verbose)) {
						print("<div class='rssdesc'>" . substr(strip_tags($rss->items[$i]['description']), 0, $char_limit) . "...</div>");
					}
					print("</li>");
				}
			} else {
				print("<li><em>No upcoming events.</em></li>");
			}

			print("</ul>");

			if ($feedlink && $rss) {
				print("<p><small><a href='" . $rss->channel['link'] . "' class='rss'><img src='http://life.umt.edu/_common/includes/rss/feed-icon-14x14.png' border='0'/> Subscribe</a></small></p>");
			}

			return true;
		}

		static function ask_monte($feed = NULL, $limit = 10, $verbose = FALSE, $feedlink = TRUE)
		{

			require_once('rss/rss_fetch.inc');

			if (!$feed) {
				echo "<p>No feed URL was specified.</p>";

				return FALSE;
			}


			$rss = @fetch_rss($feed);

			if ($rss) {

				$count = 0;

				echo "<ul class=\"rss\">";
				foreach ($rss->items as $item) {
					if ($count < $limit) {
						$href = $item['link'];
						$title = $item['title'];
						$desc = $item['description'];
						echo "<li>";
						echo "<a href=\"$href\" class=\"title\">$title</a>";
						if ($desc && $verbose)
							echo "<div class=\"rssdesc\">$desc</div>";
						echo "</li>";
						$count++;
					}
				}
			} else {
				echo "<li><em>No upcoming events.</em></li>";
			}

			echo "</ul>";

			if ($feedlink && $rss)
				echo "<p><small><a href=\"" . $rss->channel['link'] . "\" class=\"rss\"><img src=\"http://life.umt.edu/_common/includes/rss/feed-icon-14x14.png\" border=\"0\"/> Subscribe.</a></small></p>";

			return TRUE;
		}

		static function um_calendar($feed = NULL, $limit = 10, $verbose = FALSE, $link = TRUE)
		{

			require_once('rss/rss_fetch.inc');

			if (!$feed) {
				echo "<p>No feed URL was specified.</p>";

				return FALSE;
			}


			$rss = @fetch_rss($feed);

			if ($rss) {

				$count = 0;

				echo "<ul class=\"rss umcalendar\">";
				foreach ($rss->items as $item) {
					if ($count < $limit) {
						$href = $item['link'];
						$title = $item['title'];
						$desc = $item['description'];
						$date = $item['pubdate'];
						$date = strtotime($date);
						$date = date("l, F j \\a\\t g:i a", $date);
						echo "<li>";
						echo "<a href=\"$href\" class=\"title\">$title</a>";

						if ($desc && $verbose) {
							echo "<div class=\"rssdesc\">$desc</div>";
						} else {
							echo "<br/><small>$date</small>";
						}
						echo "</li>";
						$count++;
					}
				}
			} else {
				echo "<li><em>No upcoming events.</em></li>";
			}

			echo "</ul>";

			if ($link && $rss)
				echo "<p><small><a href=\"" . $rss->channel['link'] . "\">View " . $rss->channel['title'] . " calendar</a>.</small></p>";

			return TRUE;
		}

		static function um_calendar_table($feed = NULL, $limit = 10, $verbose = FALSE, $link = TRUE)
		{

			require_once('rss/rss_fetch.inc');

			if (!$feed) {
				echo "<p>No feed URL was specified.</p>";

				return FALSE;
			}


			$rss = @fetch_rss($feed);

			if ($rss) {

				$count = 0;
				echo "<table cellpadding='0' cellspacing='0' class='day' id='UNLmaincontent'>
                    <thead>
                            <tr>
                                    <th class='date'>Time</th>
                                    <th class='date'>Event Title</th>
                            </tr>
                    </thead>
                    <tbody class='vcalendar'>";
				foreach ($rss->items as $item) {
					if ($count < $limit) {
						$href = $item['link'];
						$title = $item['title'];
						$desc = $item['description'];
						$date = $item['pubdate'];
						$date = strtotime($date);
						$date = date("l, F j \\a\\t g:i a", $date);
						echo "<tr class='vevent'>
                                            <td valign='top' class='date'>" . $date . "</td>
                                            <td>
                                                <a class='url summary' href='" . $href . "'>" . $title . "</a>
                                                <blockquote class='description'>" . $desc . "</blockquote>
                                            </td>
                                        </tr>";
						$count++;
					}
				}
			} else {
				echo "<tr><td><em>No upcoming events.</em></td></tr>";
			}

			echo "</tbody></table>";

			if ($link && $rss)
				echo "<p><small><a href=\"" . $rss->channel['link'] . "\">View " . $rss->channel['title'] . " calendar</a>.</small></p>";

			return TRUE;
		}

	}
}