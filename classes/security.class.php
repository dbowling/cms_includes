<?php

if (!class_exists("security")) {
	require_once('lib/umsecurity.php');
	require_once('lib/oid.php');
	require_once('encryption.class.php');

	class security
	{

		var $umsecurity;
		var $encryption;
		var $cookie_key;

		function __construct()
		{
			$this->encryption = new Encryption();
			$this->umsecurity = new Umsecurity();
			if (isset($_GET['logout']) && $_GET['logout'] == 'true') {
				unset($_SESSION['phpCAS']['user']);
				$this->umsecurity->logout();
			}
			$this->cookie_key = $this->encryption->encrypt(md5('netid')) . "2";
		}

		public function login()
		{

			if (isset($_COOKIE[$this->cookie_key])) {
				$netid = $this->encryption->decrypt($_COOKIE[$this->cookie_key]);
			} else {
				$netid = $this->umsecurity->force_authentication();
				setcookie($this->cookie_key, $this->encryption->encrypt($netid));
			}

			return $netid;
		}

		public function logout_button($text = "Logout", $class = "logout", $return = false)
		{
			$output = "<div class='" . $class . "'><a href='?logout=true'>" . $text . "</a></div>";
			if ($return) {
				return $output;
			} else {
				print($output);
			}
		}

	}
}