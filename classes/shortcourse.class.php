<?php

if (!class_exists("shortcourse")) {
	function date_range_to_string($start_date, $end_date, $format = 'M&\nb\s\p;j')
	{
		$start_date_string = format_date($start_date, $format);
		$end_date_string = format_date($end_date, $format);

		$date_string = $start_date_string;
		if ($start_date_string != $end_date_string) {
			$date_string = $start_date_string . " to " . $end_date_string;
		} else {

		}
		return $date_string;
	}

	function to_date($sDate)
	{
		$date = DateTime::createFromFormat('M j, Y H:i:s', $sDate);
		if (!is_object($date)) {
			$date = DateTime::createFromFormat('Y-m-d', $sDate);
		}
		if (!is_object($date)) {
			$date = DateTime::createFromFormat('H:i:s', $sDate);
		}
		if (!is_object($date)) {
			$date = DateTime::createFromFormat('Y-m-d H:i:s', $sDate);
		}
		if (!is_object($date)) {
			die("cannot create '" . $sDate . "' into a DateTime Object");
		}
		return $date;
	}

	function format_date($sDate, $format)
	{
		return to_date($sDate)->format($format);
	}

	class shortcourse
	{

		var $base;

		function __construct($category_id = null)
		{
			$this->category_id = $category_id;
			$this->base = "http://www.umt.edu/it/training/courses/";
		}

		private function pull_shortcourse_data($limit = 10, $show_sections = false)
		{
			if ($this->category_id !== null) {
				$feed = $this->base . "api/home/courses/category/" . $this->category_id . "/format/json";
			} else {
				$feed = $this->base . "api/home/courses/format/json";
			}
			if (!$feed) {
				echo "<p>No feed URL was specified.</p>";

				return FALSE;
			}
			$results = json_decode(file_get_contents($feed));

			if ($show_sections) {
				$sections = array();
				for ($i = 0; $i < count($results); $i++) {
					for ($j = 0; $j < count($results[$i]->sections); $j++) {
						$course_data = clone $results[$i];
						$course_data->sections = array(clone $results[$i]->sections[$j]);
						$sections[] = $course_data;
						unset($course_data);
					}
				}
				$results = $sections;
			}


			$courses = array();
			for ($i = 0; $i < count($results); $i++) {
				$result = $results[$i];
				$earliest_section = $result->sections[0];

				foreach ($result->sections as $section) {
					$earliest_section_date = to_date($earliest_section->start_date);

					$section_date = to_date($section->start_date . ' ' . $section->start_time);

					$diff = $earliest_section_date->diff($section_date);
					$past = $section_date->diff(new DateTime());
					if ($diff->invert == 1 && $past->invert == 0) {
						$earliest_section = $section;
					}
				}

				$course = new stdClass();
				$course->section_number = $earliest_section->section_number;
				$course->course_id = $result->id;
				$course->name = $result->name . ($show_sections ? ", section ".$earliest_section->section_number : "");
				$course->description = $result->description;
				$course->start_date = $earliest_section->start_date;
				$course->end_date = $earliest_section->end_date;
				$course->start_time = $earliest_section->start_time;
				$course->end_time = $earliest_section->end_time;

				$key = format_date($course->start_date . ' ' . $course->start_time, 'U') . '.' . $course->course_id;

				$courses[$key] = $course;
			}
			ksort($courses);
			$courses = array_slice($courses, 0, $limit);
			return $courses;

		}

		function display($limit = 10, $verbose = FALSE, $show_sections = false)
		{
			$result = $this->pull_shortcourse_data($limit, $show_sections);

			echo "<ul class=\"rss\">";
			if (count($result) > 0) {

				foreach ($result as $item) {
					$href = $this->base . 'home/course/' . $item->course_id;
					$title = $item->name;
					if (!empty($item->description)) {
						$desc = $item->description;
					}
					echo "<li>";
					echo "<a href=\"$href\" class=\"title\">$title</a>";
					if ($desc && $verbose)
						echo "<div class=\"rssdesc\">$desc</div>";
					echo "</li>";
				}
			} else {
				echo "<li><em>No upcoming events.</em></li>";
			}

			echo "</ul>";

			return TRUE;
		}

		function display_table($limit = 10, $show_sections = FALSE)
		{
			$result = $this->pull_shortcourse_data($limit, $show_sections);

			print("
                <table>
                    <tbody>
                ");
			if (count($result) > 0) {
				foreach ($result as $ts => $item) {
					print("<tr>
                            <td>
                            " . date_range_to_string($item->start_date, $item->end_date) . "
                            <td>
                            <a href='" . $this->base . 'home/course/' . $item->course_id . "' class='title'>" . $item->name . "</a>
                            </td>
                            </tr>");
				}
			} else {
				echo "<tr><td><em>No upcoming events.</em></td></tr>";
			}

			echo "</tbody></table>";

			return TRUE;
		}

	}
}