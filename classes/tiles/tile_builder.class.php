<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of tiles
 *
 * @author ns159438e
 */
include('mustache.php/src/Mustache/Autoloader.php');

class tile_builder
{

	function __construct()
	{

		Mustache_Autoloader::register();
		$this->mustache = new Mustache_Engine;
	}

	private function obj_to_html($obj)
	{
		$output = "";
		foreach ($obj as $key => $value) {
			$output .= "<" . $key . ">";
			if (!is_string($value)) {
				$output .= $this->obj_to_html($value);
			} else {
				$output .= $value;
			}

			$output .= "</" . $key . ">";
		}
		return $output;
	}

	private function obj_to_array($obj)
	{
		$array = array();
		foreach ($obj as $key => $value) {
			if (is_object($value)) {
				if ($key == "content") {
					$array[$key] = $this->obj_to_html($value);
				} else {
					$array[$key] = $this->obj_to_array($value);
				}
			} else {
				$array[$key] = str_ireplace("&amp;","&",$value);
			}
		}
		return $array;
	}

	public function build($tiles, $mustache_templates, $per_row = 3)
	{
		print("<div class='tile_row row-fluid'>");
		$i = 0;
		foreach ($tiles as $tile) {
			$i++;
			$template_html = "";
			$tile->date = $this->display_date($tile->{'created_on'});
			$template_name = str_replace("featured-tiles/", "", $tile->system_data_structure->attributes->{"definition-path"});
			foreach ($mustache_templates as $template) {
				if ($template->name == $template_name) {
					$template_html = html_entity_decode($template->system_data_structure->template);
				}
			}
			//removal of span4 is here for transitionary purposes. it can be removed when the tile templates are updated
			$template_html = str_ireplace("span4", "", $template_html);

			$template_html = "<div class='span" . (12 / $per_row) . "'>" . $template_html . "</div>";

			echo $this->mustache->render($template_html, $this->obj_to_array($tile));

			if ($i % $per_row == 0 && $i != count($tiles)) {
				print("</div><div class='tile_row row-fluid'>");
			}
		}
		print("</div>");
	}

	private function display_date($timestamp, $format = "F jS, Y")
	{
		$formatted_timestamp = number_format($timestamp / 1000, 0, '', '');
		$date = date('d/m/Y', $formatted_timestamp);

		$formatted_date = date($format, $formatted_timestamp);
		if ($date == date('d/m/Y')) {
			$formatted_date = 'Today';
		} else if ($date == date('d/m/Y', mktime() - (24 * 60 * 60))) {
			$formatted_date = 'Yesterday';
		}
		if (strlen($formatted_date) > 16) {
			$formatted_date = $this->display_date($timestamp, "M jS, Y");
		}

		return $formatted_date;
	}


	public function parse_json($tile_string)
	{
		$tile = json_decode($tile_string);
		return $this->parse_data($tile);

	}

	private function parse_data($data)
	{
		if ($data != null) {
			foreach ($data as $key => $value) {
				if (!is_string($value) && is_numeric(stripos(end(array_keys((array)$value)), "Array_Value"))) {
					$data->$key = $this->convert_arrays($value);
				} else if (!is_string($value)) {
					$data->$key = null;
					$data->$key = $this->parse_data($value);
				}
			}
		}
		return $data;

	}

	private function convert_arrays($data)
	{
		if (is_numeric(stripos(end(array_keys((array)$data)), "Array_Value"))) {
			$collection = array();
			foreach ($data as $key => $value) {
				$data = new stdClass();
				$data->value = $value;
				$data->string = strtolower(str_replace(" ", "_", $value));
				$collection[] = $data;
			}
			$data = new stdClass();
			$data->count = count($collection);
			$data->values = $collection;
		}
		return $data;
	}

}