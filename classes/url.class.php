<?php
if (!class_exists('url')) {
	class url
	{
		public static function global_resources_base($path = '')
		{
			$resources_path = "_plugins";
			$plugin_hosts = array(
				"wts-phpdev1.ito.umt.edu",
				"wts-phpdev2.ito.umt.edu",
				"wts-phpdev3.ito.umt.edu",
				"wts-phpdev4.ito.umt.edu",
				"wts-phpdev5.ito.umt.edu",
				"umt.edu",
				"www.umt.edu",
				"staging.umt.edu",
				"www.staging.umt.edu",
			);
			if (!in_array($_SERVER["HTTP_HOST"], $plugin_hosts)) {
				$global_resources_base = "http://www.umt.edu/" . $resources_path;
			} else {
				$global_resources_base = url::base($resources_path);
			}
			return $global_resources_base . $path;

		}

		public static function base($path = '')
		{
			return trim((isset($_SERVER["HTTPS"]) ? 'https:' : 'http:') . "//" . trim(trim($_SERVER["HTTP_HOST"], '/') . "/" . trim($path, '/'), '/'), "/");
		}

		public static function  create_url($relative_url, $source_url = null)
		{
			if ($source_url == null) {
				$source_url = $_SERVER["SCRIPT_URI"];

			}
			$relative_pieces = explode("/", $source_url);
			if (end(explode(".", end($relative_pieces))) == "php") {
				unset($relative_pieces[count($relative_pieces) - 1]);
				$source_url = implode("/", $relative_pieces);
			}
			$array = explode('/', $source_url . '/' . $relative_url);
			$domain = array_shift($array);

			$parents = array();
			foreach ($array as $dir) {
				switch ($dir) {
					case '.':
						// Don't need to do anything here
						break;
					case '..':
						array_pop($parents);
						break;
					default:
						$parents[] = $dir;
						break;
				}
			}
			return $domain . '/' . implode('/', $parents);
		}
	}
}