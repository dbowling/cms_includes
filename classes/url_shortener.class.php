<?php

if (!class_exists("url_shortener")) {
	class url_shortener
	{

		var $domain;
		var $site_path;
		var $webservice_base;

		function __construct($domain, $ignore = "")
		{
			$this->domain = $domain;
			$this->site_path = $ignore;
			$this->webservice_base = "http://www.umt.edu/api/v1/shorturls/";
		}

		/**
		 * Prep URL
		 * from codeigniter
		 * Simply adds the http:// part if no scheme is included
		 *
		 * @param    string    the URL
		 * @return    string
		 */
		function prep_url($str = '')
		{
			if ($str === 'http://' OR $str === '') {
				return '';
			}

			$url = parse_url($str);

			if (!$url OR !isset($url['scheme'])) {
				return 'http://www.umt.edu/' . $str;
			}

			return $str;
		}

		function redirect_lookup()
		{
			$path = (!empty($_SERVER["SCRIPT_URL"]) ? $_SERVER["SCRIPT_URL"] : $_SERVER["REQUEST_URI"]);

			$segments = explode("/", trim(str_replace($this->site_path, '', $path), "/"));
			$url = $this->webservice_base . 'check/';
			$fields = array('segments' => json_encode($segments), 'domain' => $this->domain);
			$fields_string = "";

			//url-ify the data for the POST
			foreach ($fields as $key => $value) {
				$fields_string .= $key . '=' . urlencode($value) . '&';
			}
			$fields_string = rtrim($fields_string, '&');

			//open connection
			$ch = curl_init();

			//set the url, number of POST vars, POST data
			curl_setopt($ch, CURLOPT_URL, $url . "?" . $fields_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			//execute
			$result = curl_exec($ch);

			//close connection
			curl_close($ch);
			$response = json_decode($result);
			if (!empty($response->destination)) {
				header('Location: ' . $this->prep_url(htmlspecialchars_decode($response->destination.(isset($_SERVER["REDIRECT_QUERY_STRING"]) ? "?".$_SERVER["REDIRECT_QUERY_STRING"]: ""))));
				die;
			}
		}

	}
}