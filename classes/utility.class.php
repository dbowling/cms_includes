<?php
if (!class_exists('utility')) {
	class utility
	{

		/**
		 * Method: xss_protect
		 *    Purpose: Attempts to filter out code used for cross-site scripting attacks
		 * @param $data - the string of data to filter
		 * @param $strip_tags - true to use PHP's strip_tags function for added security
		 * @param $allowed_tags - a list of tags that are allowed in the string of data
		 * @return a fully encoded, escaped and (optionally) stripped string of data
		 * @author http://jstiles.com/Blog/How-To-Protect-Your-Site-From-XSS-With-PHP
		 */
		public static function xss_protect($data, $strip_tags = false, $allowed_tags = "")
		{
			if ($strip_tags) {
				$data = strip_tags($data, $allowed_tags . "<b>");
			}

			if (stripos($data, "script") !== false) {
				$result = str_replace("script", "scr<b></b>ipt", htmlentities($data, ENT_QUOTES));
			} else {
				$result = htmlentities($data, ENT_QUOTES);
			}

			return $result;
		}

		function site_url($path = "")
		{
			$protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != "off") ? "https" : "http";
			return $protocol . "://" . $_SERVER['HTTP_HOST'] . (!empty($path) ? "/" . trim($path, '/') : "");
		}

		/**
		 *  Pulled from CI
		 * @param string $urill
		 * @param string $method
		 * @param type $code
		 */
		public function redirect($uri = '', $method = 'auto', $code = NULL)
		{

			// IIS environment likely? Use 'refresh' for better compatibility
			if ($method === 'auto' && isset($_SERVER['SERVER_SOFTWARE']) && strpos($_SERVER['SERVER_SOFTWARE'], 'Microsoft-IIS') !== FALSE) {
				$method = 'refresh';
			} elseif ($method !== 'refresh' && (empty($code) OR !is_numeric($code))) {
				// Reference: http://en.wikipedia.org/wiki/Post/Redirect/Get
				$code = (isset($_SERVER['REQUEST_METHOD'], $_SERVER['SERVER_PROTOCOL'])
					&& $_SERVER['REQUEST_METHOD'] === 'POST'
					&& $_SERVER['SERVER_PROTOCOL'] === 'HTTP/1.1') ? 303 : 302;
			}

			switch ($method) {
				case 'refresh':
					header('Refresh:0;url=' . $uri);
					break;
				default:
					header('Location: ' . $uri, TRUE, $code);
					break;
			}
			exit;
		}

	}
}