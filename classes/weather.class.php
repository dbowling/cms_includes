<?php

if (!class_exists("Weather")) {
	class Weather
	{

		var $zip;
		var $config;

		function __construct($zip = '59801')
		{
			$this->zip = $zip;
			$this->config = (object)array("display" => "");
			print("<link href='/_common/includes/resources/weather.css' rel='stylesheet' type='text/css' />\n");
		}

		private function merge_config($config)
		{
			$this->config = (object)array_merge((array)$this->config, $config);
		}


		function display_current($config = array())
		{
			$this->merge_config($config);
			$output = null;
			if (function_exists('apc_fetch')) {
				$output = apc_fetch(__FUNCTION__ . $this->zip);
			}
			if ($output == null) {
				$link = 'http://www.weather.gov/xml/current_obs/KMSO.xml';
				$xml = @simplexml_load_file($link, NULL, LIBXML_NOCDATA);
				if ($xml != false) {
					$location = $xml->xpath('/current_observation/location');
					$weather = $xml->xpath('/current_observation/weather');
					$icon_url_base = $xml->xpath('/current_observation/icon_url_base');
					$icon_url_name = $xml->xpath('/current_observation/icon_url_name');
					$temp_f = $xml->xpath('/current_observation/temp_f');
					$temp_c = $xml->xpath('/current_observation/temp_c');
					$wind_string = $xml->xpath('/current_observation/wind_string');
					$visibility_mi = $xml->xpath('/current_observation/visibility_mi');
					$observation_time = $xml->xpath('/current_observation/observation_time');
					$credit = $xml->xpath('/current_observation/credit');
					$credit_url = $xml->xpath('/current_observation/credit_URL');
					$credit_image = $xml->xpath('/current_observation/image/url');

					$output = <<<EOF
            <div class='weather_widget {$this->config->display}'>
                <header>
                    <h2>Current Conditions</h2>
                    <p class='location'>For {$location[0]}</p>
                </header>
                <div class='weather' style='background-image:url({$icon_url_base[0]}{$icon_url_name[0]})' >
                    <p class='weather_string'>{$weather[0]}</p>
                    <p class='temperature'>{$temp_f[0]}&deg; F <span class='celcius'>({$temp_c[0]}&deg; C)</span></p>
                </div>
                <div class='meta'>
                    <p class='wind'>Wind: {$wind_string[0]}</p>
                    <p class='visibility'>Visibility: {$visibility_mi[0]} Miles</p>
                </div>
                <footer>
                    <p class='updated'>{$observation_time[0]}</p>
                    <p class='credit'>Provided By <a href='{$credit_url[0]}'>{$credit[0]}</a></p>
                </footer>
            </div>
EOF;
				}
			}
			print $output;
		}

	}
}