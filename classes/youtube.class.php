<?php

if (!class_exists("Youtube")) {
	class Youtube
	{

		private function get_videos($username, $count = 5)
		{
			$url = "http://gdata.youtube.com/feeds/api/users/" . $username . "/uploads";

			$ch = curl_init();

			// set URL and other appropriate options
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			// grab URL and pass it to the browser
			$response = curl_exec($ch);
			// close cURL resource, and free up system resources
			curl_close($ch);

			$xml = simplexml_load_string(str_replace(array("media:"), array("media_"), $response));

			$videos = array();
			for ($i = 0; $i < count($xml->entry) && $i <= $count; $i++) {
				$video = new stdClass();
				$attributes = "@attributes";
				$content = (array)$xml->entry[$i]->media_group->media_content;
				$youtube_page = (array)$xml->entry[$i]->link;
				$thumbnail = (array)$xml->entry[$i]->media_group->media_thumbnail;

				$video->title = (string)$xml->entry[$i]->title;
				$video->content = (string)$xml->entry[$i]->content;
				$video->thumbnail = $thumbnail['@attributes']['url'];
				$video->url = $content['@attributes']['url'];
				$video->youtube_page = $youtube_page['@attributes']['href'];

				$videos[] = $video;
			}
			return $videos;
		}

		private function embed_video($video, $height = 331, $width = 445)
		{
			return '<object width="' . $width . '" height="' . $height . '"><param name="movie" value="' . $video->url . '"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="' . $video->url . '" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="' . $width . '" height="' . $height . '"></embed></object>';
		}

		public function embed_most_recent_video($username, $width = 445, $height = 331)
		{
			$videos = $this->get_videos($username, 1);
			print($this->embed_video($videos[0], $height, $width));
		}

		public function list_videos($username, $count = 5, $show_thumbs = true, $skip_count = 0)
		{
			$videos = $this->get_videos($username, $count);
			$output = "<ul class='youtube_list'>";
			for ($i = 0; $i < $skip_count; $i++) {
				unset($videos[$i]);
			}
			foreach ($videos as $video) {
				$thumb = "";
				if ($show_thumbs == true) {
					print("<pre>");
					//var_dump($video);
					print("</pre>");
					$thumb = "<a href='" . $video->url . "'><img src='" . $video->thumbnail . "' alt='Thumbnail for " . htmlentities($video->title) . "' width='150px;'/></a>";
				}
				$output .= "<li>" . $thumb . "<p><a href='" . $video->youtube_page . "'>" . htmlentities($video->title) . "</a></p></li>";
			}
			$output .= "</ul>";
			print($output);
		}

	}
}