<?php

/* --------------------------------------------------------------------------------------
	
	Begin global auto-loader for includes

   --------------------------------------------------------------------------------------*/

	//grabbing the include file... checking all the way up the directory structure to find it
	$file = '_common/includes/master.inc.php';
	while(!file_exists($file)){
			$file = '../'.$file;
		}
	include($file);


/* --------------------------------------------------------------------------------------
	
	End global auto-loader for includes

   --------------------------------------------------------------------------------------*/

?>
<html>

<head>
	<title>Custom Functions</title>
	<style>
		h1 {color: #313131}
		h2 {border-bottom: 1px solid #ccc; width: 100%; color: #414141; font-size: 16px; margin-top: 40px;}
		
		code {background-color: #FCFCFC; color: #660066;}
		.example {border: 1px solid #ccc; background-color: #F9F9F9; padding: 15px; margin: 15px;}
		.example p {margin-top: 0;}
		
		body {
		 margin: 0;
		 padding: 30px;
		 font-family: Lucida Grande, Verdana, Geneva, Sans-serif;
		 font-size: 14px;
		 color: #333;
		 background-color: #fff;
		}
		
		a {
		 color: #0134c5;
		 background-color: transparent;
		 text-decoration: none;
		 font-weight: normal;
		}
		a:visited {
		 color: #0134c5;
		 background-color: transparent;
		 text-decoration: none;
		}

		.up {border: 1px solid #ccc; background-color: #F9F9F9; padding: 5px; margin: 15px 0;}
		.up a {font-size: 10px;}
	</style>
</head>

<body>

<h1>Examples of Custom Functions for Cascade</h1>

<h2 id="toc">Table of Contents</h2>
<ul>
	<li><a href="#autoload">Auto Loading</a></li>
	<li><a href="#rss">RSS</a></li>
	<li><a href="#randomimage">Random Image Rotation</a></li>
	<li><a href="#formbuilder">Form Builder</a></li>
</ul>
<h2 id="autoload">Auto Loading Resources</h2>

<p>In order for these functions to work, the class files must be auto loaded. Use the following code before the &lt;html&gt; opening tag:</p>

<code>
	&lt;!--#START-ROOT-CODE<br/>
	&lt;?php<br/>
	&nbsp;//grabbing the include file... checking all the way up the directory structure to find it<br/>
	&nbsp;$file = '_common/includes/master.inc.php';<br/>
	&nbsp;while(!file_exists($file)){<br/>
	&nbsp;&nbsp;$file = '../'.$file;<br/>
	&nbsp;}<br/>
	&nbsp;include($file);<br/>
?&gt;<br/> #END-ROOT-CODE--&gt;
</code>

<p class="up"><a href="#toc">&uarr; TOC</a></p>

<h2 id="rss">RSS Feeds</h2>

<p>Allows you to implement an RSS feed within a page.</p>

<p>Parameters: Feed URL (string), Feed Item Limit (integer defaults to 10), Description (defaults to false) , Link to subscribe (defaults to true)</p>

<p>Standard RSS Example:</p>
<code>

	&lt;!--#START-CODE&lt;?php <br/>
	
	&nbsp;rss::display('http://events.umt.edu/?&upcoming=upcoming&format=rss&limit=5', 4, TRUE, TRUE);<br/>
	?&gt;#END-CODE--&gt;
</code>

<link href="http://events.umt.edu/templates/default/frontend_main.css" media="screen" rel="stylesheet" type="text/css" />
<div class="example">
	<p>Example:</p>
	<?php
		rss::display('http://events.umt.edu/?&upcoming=upcoming&format=rss&limit=5', 4, TRUE, TRUE);
	?>
</div>

<p>Events.umt.edu Examples:</p>
<p>The um_calendar() function takes the same parameters as a regular RSS, but it is tweaked to always show the date of the event. It also displays a different (optional) link at the end of the feed.</p>
<code>
	&lt;!--#START-CODE&lt;?php <br/>

	&nbsp;rss::um_calendar('http://events.umt.edu/?&upcoming=upcoming&format=rss&limit=5', 4, TRUE, TRUE);<br/>
	?&gt;#END-CODE--&gt;
</code>

<div class="example">
	<p>Example:</p>
	<?php
		rss::um_calendar('http://events.umt.edu/?&upcoming=upcoming&format=rss&limit=5', 4, FALSE, TRUE);
	?>
</div>


<p>The um_calendar_table() function takes the same parameters as a regular RSS, but it is tweaked to always show the date of the event. It also displays a different (optional) link at the end of the feed.</p>
<code>
            &lt;link href="http://events.umt.edu/templates/default/frontend_main.css" media="screen" rel="stylesheet" type="text/css" /&gt;<br/>
	&lt;!--#START-CODE&lt;?php <br/>

	&nbsp;rss::um_calendar_table('http://events.umt.edu/?&upcoming=upcoming&format=rss&limit=5', 4, TRUE, TRUE);<br/>
	?&gt;#END-CODE--&gt;
</code>

<div class="example">
	<p>Example:</p>
	<?php
		rss::um_calendar_table('http://events.umt.edu/?&upcoming=upcoming&format=rss&limit=5', 4, FALSE, TRUE);
	?>
</div>

<p class="up"><a href="#toc">&uarr; TOC</a></p>

<h2 id="formbuilder">Form Builder</h2>
<p>Allows you to easily build forms in cascade</p>
<p>Parameters: File Location (string), Display name (string)</p>
<code>
    <pre>
        &lt;link href="css/email_form.css" rel="StyleSheet" type="text/css" /&gt;
	&lt;?php
	$config = array(
            "thanks"            =>"Thanks for your Response",
            "submit_text"       =>"Send",
            "required_class"    =>"required",
            "form_id"           =>"email_form",
            "ul_class"          =>"email_form",
            "action"            =>htmlentities($_SERVER['PHP_SELF']),
            "warning_class"     =>"warning",
            "from_field"        =>""
            );

        $fields = array(


            array(
                "type"=>"text",
                "name"=>"Your Name",
                "class"=>"",
                "required"=>true),
            array(
                "type"=>"text",
                "name"=>"Your Email",
                "class"=>"",
                "required"=>true),
            array(
                "type"=>"checkbox",
                "name"=>"Join the mailinglist",
                "class"=>"",
                "required"=>true),
            array(
                "type"=>"textarea",
                "name"=>"Information About Your Community",
                "class"=>"",
                "required"=>false)
        );
        $Forms = new Forms($config);
        $Forms->display_email_form($fields, "email.address@umontana.edu", $_POST);
	?&gt;
</pre>
</code>
<div class="example">
	<p>Example:</p>

        <link href="http://staging.umt.edu/communityhub/css/email_form.css" rel="StyleSheet" type="text/css" />
	<?php

	$config = array(
            "thanks"=>"",
            "from_field"=>"Your Email",
            "action"=>htmlentities($_SERVER['PHP_SELF'])."#formbuilder"
            );

        $fields = array(
            array(
                "type"=>"text",
                "name"=>"Your Name",
                "class"=>"",
                "required"=>true),
            array(
                "type"=>"text",
                "name"=>"Your Email",
                "class"=>"",
                "required"=>true),
            array(
                "type"=>"checkbox",
                "name"=>"Join the mailinglist",
                "class"=>"",
                "required"=>true),
            array(
                "type"=>"textarea",
                "name"=>"Information About Your Community",
                "class"=>"",
                "required"=>false)
        );
        $Forms = new Forms($config);
        $Forms->display_email_form($fields, "nick.shontz@umontana.edu,nickshontz@gmail.com", $_POST);
	?>
</div>

<h2 id="randomimage">Image Rotation</h2>

<p>Allows you to randomly rotate an image from a directory.</p>

<p>Parameters: Feed URL (string), Feed Item Limit (integer defaults to 10), Description (defaults to false) , Link to subscribe (defaults to true)</p>

<code>
	&lt;?php<br/>
	&nbsp;random::img('example_images');<br/>
	?&gt;
</code>

<div class="example">
	<p>Example:</p>
<?php
random::img('example_images');
?>
</div>

<p class="up"><a href="#toc">&uarr; TOC</a></p>

<h2 id="shortcourses">Shortcourse Feed</h2>

<p>Allows you to include IT Shortcourse Data</p>

<p>Parameters:Feed Item Limit (integer defaults to 10), Description (defaults to false)</p>

<code>
	&lt;?php<br/>
        $shortcourse = new shortcourse();<br/>
	$shortcourse->display(10, true);<br/>
	?&gt;
</code>

<div class="example">
	<p>Example:</p>
<?php
$shortcourse = new shortcourse();
$shortcourse->display(10, true);
?>
</div>

<p>Parameters:Feed Item Limit (integer defaults to 10)</p>

<code>
	&lt;?php<br/>
        $shortcourse = new shortcourse();<br/>
	$shortcourse->display_table($limit=10);<br/>
	?&gt;
</code>

<div class="example">
	<p>Example:</p>
<?php
$shortcourse = new shortcourse();
$shortcourse->display_table(20);
?>
</div>

<p class="up"><a href="#toc">&uarr; TOC</a></p>

</body>
</html>