<?php
/**
 ** Title: Auto Loading Resources
 ** Description: In order for these functions to work, the class files must be auto loaded. Use the following code before the &lt;html&gt; opening tag:
 ** No Example
 **/
session_start();
date_default_timezone_set('America/Denver');
/* --------------------------------------------------------------------------------------
  Begin global auto-loader for includes
  -------------------------------------------------------------------------------------- */
$file = '_common/includes/master.inc.php';
while (!file_exists($file)) {
    $file = '../' . $file;
}
include($file);
/* --------------------------------------------------------------------------------------
  End global auto-loader for includes
  -------------------------------------------------------------------------------------- */
?>