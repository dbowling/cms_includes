<?php
/**
 ** Title: Calendar Table
 ** Description: Allows you to implement an RSS feed within a page.
 ** Parameters: Feed URL (string), Feed Item Limit (integer defaults to 10), Description (defaults to false) , Link to subscribe (defaults to true)
 **/

rss::um_calendar_table('http://events.umt.edu/?&upcoming=upcoming&format=rss&limit=5', 4, FALSE, TRUE);
?>
