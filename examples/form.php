<?php
/**
 ** Title: Form Builder
 ** Description: Allows you to easily build forms in cascade
 ** Parameters: File Location (string), Display name (string)
 **/

$config = array(
    "thanks" => "",
    "from_field" => "Your Email",
    "show_clear_form" => true,
    "form_id" => "email_form",
    "captcha" => true,
    "action" => htmlentities($_SERVER['PHP_SELF']) . "?file=form#email_form"
);

$fields = array(
    array(
        "type" => "text",
        "name" => "Your Name",
        "class" => "",
        "help_text" => "What your parents named you",
        "required" => true),
    array(
        "type" => "text",
        "name" => "Your Email",
        "class" => "",
        "required" => true),
    array(
        "type" => "checkbox",
        "name" => "Join the mailinglist",
        "class" => "",
        "help_text" => "We will never share your information",
        "required" => true),
    array(
        "type" => "textarea",
        "name" => "Information About Your Community",
        "class" => "",
        "required" => false),
    array(
        "type" => "presentation",
        "name" => "Personal Information",
        "class" => "",
        "required" => false),
    array(
        "type" => "dropdown",
        "name" => "Favorite Pet",
        "options" => array(
            "Dogs",
            "Cats",
            "Bats",
        ),
        "class" => "",
        "required" => false),
    array(
        "type" => "dropdown",
        "name" => "State",
        "options" => array(),
        "class" => "",
        "required" => false),
    array(
        "type" => "dropdown",
        "name" => "Year",
        "options" => array(),
        "class" => "",
        "required" => false)
);
$Forms = new Forms($config);
$Forms->display_email_form($fields, "nick.shontz@umontana.edu,nickshontz@gmail.com", $_POST);
?>