<?php
session_start();

date_default_timezone_set('America/Denver');
/* --------------------------------------------------------------------------------------
  Begin global auto-loader for includes
  -------------------------------------------------------------------------------------- */
$file = '_common/includes/master.inc.php';
while (!file_exists($file)) {
    $file = '../' . $file;
}
include($file);
?>
<html>

    <head>
        <title>Custom Functions</title>

        <link href="../resources/style.css" rel="StyleSheet" type="text/css" />
        <link href="../resources/email_form.css" rel="StyleSheet" type="text/css" />

        <link rel="stylesheet" type="text/css" media="screen" href="http://events.umt.edu/templates/default/frontend_main.css" /> 
    </head>

    <body>
        <h2><a href="../">Return to function list</a></h2>
        <?php
        /* --------------------------------------------------------------------------------------
          End global auto-loader for includes
          -------------------------------------------------------------------------------------- */
        if (!empty($_GET['file']) && count(explode('/', $_GET['file'])) == 1) {
            $file = $_GET['file'] . ".php";
            $php_code = trim(str_replace(array("<?php", "?>"), "", file_get_contents($file)));
            ?>
            <div class="code">
                <p>Example Code:</p>
                <?php print(highlight_string("<?php\n" . $php_code . "\n?>", true)); ?>
            </div>

            <div class="example">
                <p>Example Output:</p>
                <?php
                include($file);
                ?>
            </div>
            <?php
        }
        ?>

    </body>

    <script src="http://www.google.com/jsapi" type="text/javascript" ></script>
    <script type="text/javascript">google.load('jquery', '1.5');</script>
    <script type="text/javascript"> 
        $(document).ready(function(){
            $("h2.title").each(function(){
                $(".toc").append("<li><a href='#"+$(this).attr("id")+"'>"+$(this).attr("id").replace(/_/g, " ")+"</a></li>");
            });
        });
    </script> 
</html>