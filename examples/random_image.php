<?php
/**
 ** Title: Image Rotation
 ** Description: Allows you to randomly rotate an image from a directory.
 ** Parameters: Feed URL (string), Feed Item Limit (integer defaults to 10), Description (defaults to false) , Link to subscribe (defaults to true)
 **/

random::img('examples/images');
?>
