<?php
/**
 ** Title: RSS Feeds
 ** Description: Allows you to implement an RSS feed within a page.
 ** Parameters: Feed URL (string), Feed Item Limit (integer defaults to 10), Description (defaults to false) , Link to subscribe (defaults to true), Display full text (defaults to FALSE), char limit for full text (defaults to 250)
 **/

rss::display('http://feeds.feedburner.com/umitannouncements', 4, TRUE, TRUE, TRUE,250);
?>
