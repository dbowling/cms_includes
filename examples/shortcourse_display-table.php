<?php
/**
 ** Title: Shortcourse Table
 ** Description: Allows you to display shortcourses as a table
 ** Parameters: Feed Item Limit (integer defaults to 10)
 **/

$shortcourse = new shortcourse(14);
$shortcourse->display_table(20);