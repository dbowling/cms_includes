<?php
/**
 ** Title: Shortcourse List
 ** Description: Allows you to list shortcourses
 ** Parameters: Feed Item Limit (integer defaults to 10), Description (defaults to false)
 **/

$shortcourse = new shortcourse();
$shortcourse->display(10, true);
?>