<?php
/**
 ** Title: Embed Youtube videos
 ** Description: Allows you to embed youtube videos
 ** Parameters: channel username, number to show, show thumbnails, and number of videos to skip
 **/

$youtube = new Youtube();

$youtube->list_videos('UniversityOfMontana',5, true,1);

?>
