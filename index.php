<html>

    <head>
        <title>Custom Functions</title>

        <link href="resources/style.css" rel="StyleSheet" type="text/css" />
        <link href="resources/email_form.css" rel="StyleSheet" type="text/css" />

        <link rel="stylesheet" type="text/css" media="screen" href="http://events.umt.edu/templates/default/frontend_main.css" /> 
    </head>

    <body>

        <h1>Examples of Custom Functions for Cascade</h1>

        <h2 id="toc">Table of Contents</h2>
        <ul class="toc">
        </ul>
        <?php
        $path = "examples";
        $myDirectory = opendir("./" . $path);

        while ($entryName = readdir($myDirectory)) {
            $dirArray[] = $entryName;
        }
        sort($dirArray);

        closedir($myDirectory);
        $start_bench = microtime(true);
        $bench = array();
        for ($i = 0; $i < count($dirArray); $i++) {
            if ($dirArray[$i] != 'index.php' && substr($dirArray[$i], 0, 1) != "." && !is_dir($path . '/' . $dirArray[$i])) {


                $file = file($path . '/' . $dirArray[$i]);

                $end_comment = false;
                $show_example = true;

                for ($j = 0; $j < count($file) && $end_comment == false; $j++) {
                    if (is_numeric(stripos($file[$j], "title"))) {
                        $parts = explode(":", $file[$j]);
                        $example_id = str_replace(" ", "_", trim($parts[1]));
                        print("<h2 class='title' id='" . $example_id . "'><a href='examples?file=" . str_replace(".php", "", $dirArray[$i]) . "'>" . $parts[1] . "</a></h2>");
                    } else if (is_numeric(stripos($file[$j], "description"))) {
                        $parts = explode(":", $file[$j]);
                        print("<p>" . $parts[1] . "</p>");
                    } else if (is_numeric(stripos($file[$j], "parameters"))) {
                        $parts = explode(":", $file[$j]);
                        print("<p>Parameters: " . $parts[1] . "</p>");
                    } else if (is_numeric(stripos($file[$j], "no example"))) {
                        $show_example = false;
                    } else if (is_numeric(stripos($file[$j], "*/"))) {
                        $end_comment = true;
                    }
                    if (is_numeric(stripos($file[$j], "**"))) {
                        $file[$j] = "";
                    }
                }


                $php_code = trim(str_replace(array("<?php", "?>"), "", implode(" ", $file)));
                ?>

                <div class="code">
                    <p>Example Code:</p>
                    <?php print(highlight_string("<?php\n" . $php_code . "\n?>", true)); ?>
                </div>
                <?php if ($show_example) { ?>
                    <div class="example">
                        <p>Example Output:</p>
                        <?php
                    }
                    include($path . '/' . $dirArray[$i]);
                    //eval ($php_code); 
                    if ($show_example) {
                        ?>
                    </div>
                <?php } ?>
                <p class='up'><a href='#toc'>&uarr; TOC</a></p>
                <?php
                $bench[$example_id] = microtime(true);
            }
        }
        ?>
        <table>
            <tr>
                <td>Example</td>
                <td>Time To Render</td>
            </tr>
            <?php
            $previos_timestamp = $start_bench;
            foreach ($bench as $file => $timestamp) {
                $diff = $timestamp - $previos_timestamp;
                $previos_timestamp = $timestamp;
                ?>
                <tr>
                    <td><?php print(str_replace('_', ' ', $file)); ?></td>
                    <td><?php print(number_format($diff, 4)); ?></td>
                </tr> 
            <?php } ?>
        </table>


    </body>

    <script src="http://www.google.com/jsapi" type="text/javascript" ></script>
    <script type="text/javascript">google.load('jquery', '1.5');</script>
    <script type="text/javascript"> 
        $(document).ready(function(){
            $("h2.title").each(function(){
                $(".toc").append("<li><a href='#"+$(this).attr("id")+"'>"+$(this).attr("id").replace(/_/g, " ")+"</a></li>");
            });
        });
    </script> 
</html>